<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use DB;
use Session;
use View;

class HomeController extends Controller
{

    public function index(){
        $session = $this->check_session();
        
        if($session['status']==1){
            return $session['redirect'];
        }else{
            $status =   DB::select(
                            DB::raw(
                                "SELECT a.*, b.* 
                                FROM status a 
                                INNER JOIN user b ON a.id_user = b.id_user
                                ORDER BY a.date DESC"
                            )
                        );

            $id_user = Session::get('id_user');
            return view('home')->with('status', $status)->with('id_user', $id_user);
        }
    }

    public function ajax_update_status(Request $req){
        $session = $this->check_session();
        
        if($session['status']==1){
            return $session['redirect'];
        }else{
            $id_user = Session::get('id_user');
            $status = $req->status;

            $update = DB::table('status')->insert([
                'id_user'   => $id_user,
                'status'    => $status,
                'date'      => date("Y-m-d H:i:s")
            ]);

            if($update){
                $get_status =   DB::select(
                                    DB::raw(
                                        "SELECT a.*, b.* 
                                        FROM status a 
                                        INNER JOIN user b ON a.id_user = b.id_user
                                        ORDER BY a.date DESC"
                                    )
                                );
                
                $view = View::make('status', [
                            'status'    => $get_status,
                            'id_user'   => $id_user
                        ]);
                $html_status = $view->render();

                $data['status'] = $html_status;
                return $data;
            }
        }
    }
}
