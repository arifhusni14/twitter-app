<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Http\Request;
use Validator;
use Session;
use Auth;
use DB;

class LogController extends BaseController
{

    public function login(Request $req){
        $email = $req->input('email');
        $password = $req->input('password');
        $msg = '';

    	$checkLogin = DB::table('user')->where(['email'=>$email, 'password'=>$password])->get();
        
    	if(count($checkLogin)>0){
            Session::put('id_user', $checkLogin[0]->id_user);
    		return redirect('/');
    	}else{
            Session::flash('msg', "The email and password you entered don't match!");
    		return redirect('/login');
    	}
    }

    public function register(Request $req){
    	$email = $req->input('email');
    	$name = $req->input('name');
    	$password = $req->input('password');

    	$register = DB::table('user')->insert([
    		'email' 	=> $email,
    		'name' 		=> $name,
            'password'  => $password,
    		'photo' 	=> 'default.jpg'
    	]);

    	if($register){
    		Session::flash('msg_register', "Registration success!");
    		return redirect('/login');
    	}
    }

    public function logout(){
        Auth::logout();
        Session::flush();
        return redirect('/login');
    }
}
