<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use DB;
use Session;
use View;
use Input;
use Storage;
use File;

class ProfileController extends Controller{

    public function index(){
        $session = $this->check_session();
        
        if($session['status']==1){
            return $session['redirect'];
        }else{
            $id_user = Session::get('id_user');
            $profile =   DB::select(
                            DB::raw(
                                "SELECT a.* 
                                FROM user a
                                WHERE a.id_user = ".$id_user
                            )
                        );

            return view('profile')->with('profile', $profile); 
        }
    }

    public function update_profile(Request $request){
        $session = $this->check_session();
        
        if($session['status']==1){
            return $session['redirect'];
        }else{
            $id_user    = Session::get('id_user');
            $name       = $request->input('name');
            $email      = $request->input('email');
            $password   = $request->input('password');
            $msg        = '';

            $data = array(
                        'name'      => $name,
                        'email'     => $email,
                        'password'  => $password
                    );

            /* Upload Image */
            if($request->hasFile('photo_file')){
                $size   = $request->file('photo_file')->getClientSize();
                if($size <= 4194304){
                    $file           = $request->file('photo_file');

                    $ext            = strtolower($file->getClientOriginalExtension());
                    $file_name      = 'pp_' . $id_user . '.' . $ext;
                    $data['photo']  = $file_name;
                    
                    $resizedFile = 'public/img/'.$file_name;
                    $this->resize_image(null , file_get_contents($file), 200 , 200 , false , $resizedFile , false , false ,100 );
                }else{
                    $msg = "File size max 4 MB";
                    Session::flash('msg', $msg);
                    return redirect('/profile');
                }
            }

            /* Update Table */
            $update = DB::table('user')
                ->where('id_user', $id_user)
                ->update($data);

            Session::flash('msg', "Update profile success!");
            return redirect('/profile');
        }
    }

}
