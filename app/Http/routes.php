<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');

Route::get('/login', function(){ return view('login')->with('err_no', 2);});
Route::post('login', 'LogController@login');
Route::get('logout', 'LogController@logout');
Route::post('register', 'LogController@register');

Route::post('update_status', 'HomeController@ajax_update_status');

Route::get('profile', 'ProfileController@index');
Route::post('update_profile', 'ProfileController@update_profile');
