<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('status', function ($table) {
            $table->increments('id_status');
            $table->integer('id_user');
            $table->string('status', 114);
            $table->dateTime('date');
            $table->timestamps();
        });

        Schema::table('status', function($table) {
            $table->foreign('id_user')->references('id_user')->on('user');
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('status');
    }
}
