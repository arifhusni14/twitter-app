$(document).ready(function(){
	$('textarea[name="status"]').val('');
	$('textarea[name="status"]').focus();
});

$(document).keypress(function(e) {
    if(e.which == 13) {
        update_status();
    }
});

function update_status(){
	var status = $('textarea[name="status"]').val();
	var id_user = $('input[name="id_user"]').val();

	if(status!=''){
		$.ajax({
			url: myHost + 'update_status',
			data: {
				id_user	: id_user,
				status 	: status
			},
			dataType: "json",
			type: 'POST',
			success : function(data){ 
				$('textarea[name="status"]').val('');
				$('#con_status').html(data.status);
			}
		});
	}
}

$('#btn_update').click(function(){
	update_status();
});
