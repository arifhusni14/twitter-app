// $(document).ready(function(){
// 	$("#form_update").bootstrapValidator({
// 		message: 'This value is not valid',
// 		fields: {
// 			email: {
// 				validators: {
// 					notEmpty: {
// 						message: 'Email is required'
// 					},
// 					regexp: {
// 			          	regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
// 			          	message: 'Email format is wrong'
// 			        }
// 				}
// 			},
// 			name: {
// 				validators: {
// 					notEmpty: {
// 						message: 'Name is required'
// 					},
// 					stringLength: {
// 						enabled: true,
// 						min: 5,
// 						message: 'Name must have at least 5 characters'
// 					}
// 				}
// 			},
// 			password: {	
// 				validators: {
// 					notEmpty: {
// 						message: 'Password is required'
// 					},
// 					stringLength: {
// 						enabled: true,
// 						min: 6,
// 						message: 'Password must have at least 6 characters'
// 					}
// 				}
// 			},
// 		}
// 	});

// 	$('#form_update').bootstrapValidator('revalidateField', 'name');
// 	$('#form_update').bootstrapValidator('revalidateField', 'email');
// 	$('#form_update').bootstrapValidator('revalidateField', 'password');
// });