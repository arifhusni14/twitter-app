@extends('layouts.app')    

@section('head')
    <link rel="stylesheet" href="{{ url('public/css/home.css') }}">
@endsection

@section('nav')
    <div class="col-xs-3 col-sm-4 col-md-4 col-lg-4">
        <div class="dropdown pull-right">
            <a href="#" class="dropdown-toggle" type="button" data-toggle="dropdown">
                <span class="fa fa-bars fa-2x" style="margin-top: 3px"></span>
            </a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{ url('/profile') }}">
                        <span class="fa fa-user"></span> Profil
                    </a>
                </li>
                <li>
                    <a href="{{ url('/logout') }}">
                        <span class="fa fa-power-off"></span> Logout
                    </a>
                </li>
            </ul>
        </div> 
    </div>
@endsection

@section('content')
    <input type="hidden" name="id_user" value="{{ $id_user }}" />

    <!-- UPDATE STATUS -->
    <div id="twt-status" class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <textarea name="status" class="form-control" placeholder="Update status..." style="resize: none">
                </textarea>
                <br />
                <button id="btn_update" class="btn btn-primary pull-right">Update</button>
            </div>
        </div>
    </div>

    <!-- STATUS -->
    <div id="twt-content" class="container-fluid">
        <div class="row con-friend-status">
            <div id="con_status" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                @foreach($status as $s)
                    @if($s->id_user != $id_user)
                        <div class="friend-status">
                            <div class="row valign-center">
                                <div class="col-xs-3 col-sm-2 col-md-1 col-lg-1">
                                    <img src="{{ url('public/img/'.$s->photo) }}" class="img-circle foto" />
                                </div>
                                <div class="col-xs-9 col-sm-10 col-md-11 col-lg-11">
                                    <strong>{{ $s->name }}</strong>
                                    <p>{{ $s->status }}</p>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="my-status">
                            <div class="row valign-center">
                                <div class="col-xs-9 col-sm-10 col-md-11 col-lg-11 text-right">
                                    <strong>{{ $s->name }}</strong>
                                    <p>{{ $s->status }}</p>
                                </div>
                                <div class="col-xs-3 col-sm-2 col-md-1 col-lg-1">
                                    <img src="{{ url('public/img/'.$s->photo) }}" class="img-circle foto" />
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ url('public/js/home.js') }}"></script>
@endsection