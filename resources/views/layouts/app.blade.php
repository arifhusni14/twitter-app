<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Twitter</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}" />

        <link rel="stylesheet" href="{{ url('public/bootstrap/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ url('public/css/common.css') }}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        @yield('head')

    </head>
    <body class="hold-transition skin-red sidebar-mini">
        <!-- HEADER -->
        <div id="twt-header" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
            <div class="col-xs-9 col-sm-4 col-sm-offset-4 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">
                <h4>Twitter Application</h4>
            </div>
            @yield('nav')

        </div>
        @yield('content')

        <script src="{{ url('public/cdn/jquery.min.js') }}"></script>
        <script src="{{ url('public/bootstrap/js/bootstrap.min.js') }}"></script>
        <script src="{{ url('public/js/common.js') }}"></script>
        @yield('js')

    </body>
</html>