@extends('layouts.app')

@section('head')
    <link rel="stylesheet" href="{{ url('public/css/login.css') }}">
@endsection

@section('content')
    <div id="twt-content">
        <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
            <h3 class="text-muted"><strong>LOGIN</strong></h3>
            @if (Session::has('msg'))
                <div class="alert alert-danger">{{ Session::get('msg') }}</div>
            @endif
            <form id="form_login" action="{{ url('/login') }}" method="post" role="form">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <input name="email" type="email" class="form-control" placeholder="Email"/>
                </div>
                <div class="form-group">
                    <input name="password" type="password" class="form-control" placeholder="Password" />
                </div>
                <center><input type="submit" value="Login" class="btn btn-info" /></center>
            </form>
            <hr />

            <h3 class="text-muted"><strong>REGISTER</strong></h3>
            @if (Session::has('msg_register'))
                <div class="alert alert-success">{{ Session::get('msg_register') }}</div>
            @endif
            <form id="form_register" action="{{ url('/register') }}" method="post" role="form">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <input name="email" type="email" class="form-control" placeholder="Email" />
                </div>
                <div class="form-group">
                    <input name="name" type="text" class="form-control" placeholder="Name" />
                </div>
                <div class="form-group">
                    <input name="password" type="password" class="form-control" placeholder="Password" />
                </div>
                <center><input type="submit" value="Register" class="btn btn-info" /></center>
            </form>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="{{ url('public/bootstrapValidator/js/bootstrapValidator.js') }}"></script>
    <script type="text/javascript" src="{{ url('public/js/login.js') }}"></script>
@endsection