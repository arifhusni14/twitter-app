@extends('layouts.app')

@section('head')
        <link rel="stylesheet" href="{{ url('public/css/login.css') }}">
        <link rel="stylesheet" href="{{ url('public/css/profile.css') }}">
@endsection

@section('nav')
    <div class="col-xs-3 col-sm-4 col-md-4 col-lg-4">
        <div class="dropdown pull-right">
            <a href="#" class="dropdown-toggle" type="button" data-toggle="dropdown">
                <span class="fa fa-bars fa-2x" style="margin-top: 3px"></span>
            </a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{ url('/') }}">
                        <span class="fa fa-home"></span> Home
                    </a>
                </li>
                <li>
                    <a href="{{ url('/logout') }}">
                        <span class="fa fa-power-off"></span> Logout
                    </a>
                </li>
            </ul>
        </div> 
    </div>
@endsection

@section('content')
    <div id="twt-content">
        @if (Session::has('msg'))
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="alert alert-success">{{ Session::get('msg') }}</div>
            </div>
        @endif
        <form id="form_update" role="form" action="{{ url('update_profile') }}" method="POST" enctype="multipart/form-data">
            <div class="col-xs-3  col-sm-3 col-sm-offset-1 col-md-2 col-md-offset-1 col-lg-2 col-lg-offset-2 text-center">
                <img src="{{ url('public/img/'.$profile[0]->photo) }}" class="img-circle foto" />
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="file" name="photo_file" style="margin-top: 10px" /><br />
            </div>
            <div id="con_data" class="col-xs-8 col-sm-7 col-md-8 col-lg-6">
                <h3 class="text-muted text-left"><strong>PROFIL</strong></h3><br />
                    <div class="form-group">
                        <input name="name" type="text" class="form-control" placeholder="Name" value="{{ $profile[0]->name }}" />
                    </div>
                    <div class="form-group">
                        <input name="email" type="email" class="form-control" placeholder="Email" value="{{ $profile[0]->email }}" />
                    </div>
                    <div class="form-group">
                        <input name="password" type="password" class="form-control" placeholder="Password" value="{{ $profile[0]->password }}" />
                    </div>
                    <input type="submit" name="submit" value="Save" class="btn btn-primary pull-right" />
            </div>
        </form>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="{{ url('public/bootstrapValidator/js/bootstrapValidator.js') }}"></script>
    <script type="text/javascript" src="{{ url('public/js/profile.js') }}"></script>
@endsection