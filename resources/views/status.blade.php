@foreach($status as $s)
    @if($s->id_user != $id_user)
        <div class="friend-status">
            <div class="row valign-center">
                <div class="col-xs-3 col-sm-2 col-md-1 col-lg-1">
                    <img src="{{ url('public/img/'.$s->photo) }}" class="img-circle " width="100%" height="100%" />
                </div>
                <div class="col-xs-9 col-sm-10 col-md-11 col-lg-11">
                    <strong>{{ $s->name }}</strong>
                    <p>{{ $s->status }}</p>
                </div>
            </div>
        </div>
    @else
        <div class="my-status">
            <div class="row valign-center">
                <div class="col-xs-9 col-sm-10 col-md-11 col-lg-11 text-right">
                    <strong>{{ $s->name }}</strong>
                    <p>{{ $s->status }}</p>
                </div>
                <div class="col-xs-3 col-sm-2 col-md-1 col-lg-1">
                    <img src="{{ url('public/img/'.$s->photo) }}" class="img-circle " width="100%" height="100%" />
                </div>
            </div>
        </div>
    @endif
@endforeach